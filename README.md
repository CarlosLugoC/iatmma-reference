# IATMMA Reference

this git contains the references of the document Identifying Advanced Transactional Models for Microservices Architecture: A Systematic Mapping Study.

References
1. Abdouli, M., Sadeg, B., Amanton, L., Alimi, A.: A system supporting nested
transactions in DRTDBSs. In: Yang, LT and Rana, OF and DiMartino, B and
Dongarra, J (ed.) Lecture Notes in Computer Science (including subseries Lecture Notes in Artificial Intelligence and Lecture Notes in Bioinformatics). Lecture Notes in Computer Science, vol. 3726 LNCS, pp. 888{897. Second Univ Naples;
Lecture Notes Comp Sci (2005)
2. Ahamad, M., Chelliah, M.: Flexible robust programming in distributed object
systems. IEEE Transactions on Knowledge and Data Engineering 14(5), 1126{
1140 (sep 2002)
3. Almanasreh, E., Moles, R., Chen, T.F.: Evaluation of methods used for estimating
content validity. Research in Social and Administrative Pharmacy 15(2), 214{221
(2019)
4. Ammann, P., Jajodia, S., Ray, I.: Applying Formal Methods to Semantic-Based
Decomposition of Transactions (1997)
5. Anwar, E., Chakravarthy, S., Viveros, M.: Realizing Transaction Models: An Extensible Approach using ECA Rules. Tech. rep. (1995)
6. Artamonov, I.: Business transactions: properties and features (2012)
7. Artishchev, S., Weigand, H.: Interoperable transactions for e-business. IFAC Proceedings Volumes (IFAC-PapersOnline) 16(1), 88{93 (2005)
8. Assimakopoulos, N.A.: Systemic process management environment for building
distributed locational systems. Journal of High Technology Management Research
11(2), 275{293 (2000)
9. Baral, C., Loboy, J.: A language to reason about long duration transactions and
their concurrent execution (1995)
10. Barghouti, N.S., Kaiser, G.E.: Concurrency Control in Advanced Database Applications. ACM Computing Surveys (CSUR) 23(3), 269{317 (1991)
11. Ben Lakhal, N., Kobayashi, T., Yokota, H.: FENECIA: Failure endurable nestedtransaction based execution of composite Web services with incorporated state
analysis. VLDB Journal 18(1), 1{56 (jan 2009)
12. Bertino, E., Catania, B., Ferrari, E.: A Nested Transaction Model for Multilevel
Secure Database Management Systems (2001)
13. Bertino, E., Jajodia, S., Mancini, L., Ray, I.: Advanced transaction processing in
multilevel secure file stores (1998)
14. Bhiri, S., Gaaloul, W., Godart, C., Perrin, O., Zaremba, M., Derguech, W.: Ensuring customised transactional reliability of composite services. Journal of Database
Management 22(2), 64{92 (apr 2011)
15. Biliris, A., Dar, S., Gehani, N., Jagadish, H.V., Ramamritham, K.: ASSET: A
System for Supporting Extended Transactions. ACM SIGMOD Record 23(2),
44{54 (1994)
16. Braun, S., Desloch, S.: A Classification of Replicated Data for the Design of
Eventually Consistent Domain Models. Proceedings - 2020 IEEE International
Conference on Software Architecture Companion, ICSA-C 2020 pp. 33{40 (2020)
17. Bukhres, O., K¨uhn, E.: Highly available and reliable communication protocols for
heterogeneous systems. Information Sciences - Applications 3(1), 1{40 (1995)
18. Canals, G., Godart, C., Charoy, F., Molli, P., Skaf, H.: COO approach to support
cooperation in software developments (1998)
19. Chen, F., Rong, X., Deng, P., Ma, S.: Transaction in large-scale device collaborative system. Journal of Computers 5(8), 1204{1212 (2010)
20. Choi, I., Park, C., Lee, C.: Task net: Transactional workflow model based on
colored Petri net. European Journal of Operational Research 136(2), 383{402
(2002)
21. Chrysanthis, P.K., Ramamritham, K.: ACTA: The SAGA continues. Tech.
Rep. 1992 (1992)
22. Chrysanthis, P.K., Ramamritham, K.: Synthesis of Extended Transaction Models
Using ACTA. ACM Transactions on Database Systems (TODS) 19(3), 450{491
(1994)
23. Cobb, E.E.: The impact of object technology on commercial transaction processing. VLDB Journal 6(3), 173{190 (1997)
24. Colombo, C., Pace, G.J.: Recovery within long-running transactions (2013)
25. Dahav, B., Etzion, O.: Distributed enforcement of integrity constraints. Distributed and Parallel Databases 13(3), 227{249 (may 2003)
26. Dan, A., Parr, F.: The Coyote approach for network centric service applications:
Conversational service transactions, a monitor and an application style (1998)
27. Darsana, C.S., Varghese, S.M.: Deadlockfree Pipeline Processing Model for Long
Transactions
28. De Ferreira Rezende, F., H¨arder, T.: Exploiting abstraction relationships’ semantics for transaction synchronization in KBMSs. Data and Knowledge Engineering
22(3), 233{259 (1997)
29. De Silva, F.M.A., Krause, S.: A distributed transaction model based on mobile
agents. In: Rothermel, K and PopescuZeletin, R (ed.) Lecture Notes in Computer Science (including subseries Lecture Notes in Artificial Intelligence and
Lecture Notes in Bioinformatics). Lecture Notes in Computer Science, vol. 1219,
pp. 198{209. Gesell Informat; Informat & Kommunikationstechnol Verbund e V;
DeTeBerkom; Int Federat Informat Proc; GMD Fokus; Tech Univ Berlin; Univ
Stuttgart (1997)
30. Devirmi¸s, T., Ulusoy, O.: Design and Evaluation of a New Transaction Execution ¨
Model for Multidatabase Systems. Information Sciences 102(1-4), 203{238 (nov
1997)
31. Di Francesco, P., Lago, P., Malavolta, I.: Architecting with microservices: A
systematic mapping study. Journal of Systems & Software 150, 77{97 (2019).
https://doi.org/10.1016/j.jss.2019.01.001
32. Doˇgdu, E.: Scheduling adaptive transactions in real-time databases. In: Wagner,
RR and Thoma, H (ed.) Lecture Notes in Computer Science (including subseries
Lecture Notes in Artificial Intelligence and Lecture Notes in Bioinformatics). Lecture Notes in Computer Science, vol. 1134 LNCS, pp. 130{142. Univ Zurich;
DEXA Assoc; Swiss Informaticians Soc; Austrian Comp Soc; GI; Res Inst Appl
Knowledge Proc (1996)
33. Dyba, T., Kitchenham, B.A., Jorgensen, M.: Evidence-based software engineering
for practitioners. IEEE software 22(1), 58{65 (2005)
34. Elmagarmid, A.K., Kuhn, E., Kuhn, O.: Implementation of the Flex Transaction
Model. Bulletzn of the IEEE Technical Committee on Data Engsneering 16(2),
28{32 (1993)
35. Elmagarmid, A.K., Rusinkiewicz, M.: Critical issues in multidatabase systems.
Information Sciences 57-58(C), 403{424 (1991)
36. Eugster, P., Vaucouleur, S.: Composing atomic features. Science of Computer
Programming 63(2), 130{146 (2006)
37. Fabry, J., Tanter, E., D’Hondt, T.: KALA: Kernel aspect language for advanced ´
transactions. Science of Computer Programming 71(3), 165{180 (2008)
38. Fekete, A., Greenfield, P., Kuo, D., Jang, J.: Transactions in loosely coupled
distributed systems (2003)
39. Felber, P., Gramoli, V., Guerraoui, R.: Elastic transactions. Journal of Parallel
and Distributed Computing 100, 103{127 (2017)
40. Garcia-Molina, H.: Modeling Long-Running Activities as Nested Sagas (1991)
41. GEORGAKOPOULOS, D., HORNICK, M.F.: a Framework for Enforceable Specification of Extended Transaction Models and Transactional Workflows (1994)
42. Georgakopoulos, D., Hornick, M.F., Manola, F.: Customizing transaction models
and mechanisms in a programmable environment supporting reliable workflow
automation. IEEE Transactions on Knowledge and Data Engineering 8(4), 630{
649 (1996)
43. Gesvindr, D., Davidek, J., Buhnova, B.: Design of scalable and resilient applications using microservice architecture in PaaS cloud. ICSOFT 2019 - Proceedings
of the 14th International Conference on Software Technologies pp. 619{630 (2019)
44. Graefe, G., Lomet, D.B., Hsu, M.: Editorial Board (Associate Editors) (2021)
45. Grefen, P., Vonk, J.: A taxonomy of transactional workflow support (2006)
46. Grefen, P., Vonk, J., Apers, P.: Global transaction support for workflow management systems: From formal specification to practical implementation. VLDB
Journal 10(4), 316{333 (dec 2001)
47. Grefen, P.W.P.J.: A Taxonomy for Transactional Workflows
48. Gropengießer, F., Hose, K., Sattler, K.U.: An extended transaction model for cooperative authoring of XML data. Computer Science - Research and Development
24(1-2), 85{100 (2009)
49. H¨arder, T., Rothermel, K.: Concurrency control issues in nested transactions.
Tech. Rep. 1 (1993)
50. Hasselbring, W., Steinacker, G.: Microservice Architectures for Scalability, Agility
and Reliability in E-Commerce. In: 2017 IEEE International Conference on Software Architecture Workshops (ICSAW). pp. 243{246 (2017)
51. H´erault, C., Nemchenko, S., Lecomte, S.: A component-based transactional service, including advanced transactional models. In: Ramos, FF and Rosillo, VL and
Unger, H (ed.) Lecture Notes in Computer Science (including subseries Lecture
Notes in Artificial Intelligence and Lecture Notes in Bioinformatics). LECTURE
NOTES IN COMPUTER SCIENCE, vol. 3563 LNCS, pp. 545{556 (2005)
52. Hewett, R., Kijsanayothin, P.: Privacy and Recovery in Composite Web Service
Transactions (2010)
53. Huang, T., Ding, X., Wei, J.: An application-semantics-based relaxed transaction
model for internetware. Science in China, Series F: Information Sciences 49(6),
774{791 (dec 2006)
54. Jafar, H., Rimiru, R.M., Kimwele, M.W.: An Alternative Model to Overcoming
Two Phase Commit Blocking Problem. International Journal of Computer (2017)
55. Jagannathan, S., Vitek, J., Welc, A., Hosking, A.: A transactional object calculus. Science of Computer Programming 57(2), 164{186 (2005),
http://www.sciencedirect.com/science/article/pii/S016764230500033X
56. Jim´enez-Peris, R., Pati~no-Mart´ınez, M.: Middleware Technologies for Adaptive
and Composable Distributed Components ADAPT: Middleware Technologies for
Adaptive and Composable Distributed Components. Tech. rep. (1998)
57. Kaiser, G.E.: Flexible transaction model for software engineering. Tech. rep.
(1990)
58. Kamath, M., Ramamritham, K.: Correctness issues in workflow management.
Distributed Systems Engineering 3(4), 213{221 (1996)
59. Kangsabanik, P., Mall, R., Majumdar, A.K.: Semantic Based Concurrency Control of Open Nested Transactions in Active Object Oriented Database Management Systems. Distributed and Parallel Databases 8(2), 181{222 (2000)
60. Kangsabanik, P., Yadav, D.S., Mall, R., Majumdar, A.K.: Performance analysis of long-lived cooperative transactions in active DBMS. Data and Knowledge
Engineering 62(3), 547{577 (2007)
61. Kikuchi, S., Bhalla, S.: Optimizing a Long-Lived Transaction with Verification
Function (2019)
62. Kiringa, I.: Specifying active databases as non-Markovian theories of actions.
Journal of Intelligent Information Systems 32(2), 105{138 (apr 2009)
63. Kiringa, I., Gabaldon, A.: Synthesizing advanced transaction models using the
situation calculus. Journal of Intelligent Information Systems 35(2), 157{212 (oct
2010)
64. Kitchenham, B.A., Dyba, T., Jorgensen, M.: Evidence-based software engineering.
In: Proceedings of the 26th international conference on software engineering. pp.
273{281. IEEE Computer Society (2004)
65. Kleissner, K., Klein, J., Salem, K., Garcia-Molina, H., Gawlick, D.: Coordinating
multi-transaction activities
66. Knoche, H.h.i.u.k.d., Hasselbring, W.: Drivers and barriers for microservice adoption - a survey among professionals in germany. Enterprise Modelling & Information Systems Architectures 14(1), 1{35 (2019)
67. Kratz, B.: Protocols For Long Running Business Transactions (2004)
68. Kratz, B., Wang, T., Vonk, J., Grefen, P.: Flexible business transaction composition in service-oriented environments (2005)
69. Krishnakumar, N., Bernstein, A.J.: Bounded Ignorance: A Technique for Increasing Concurrency in a Replicated System (1994)
70. Kudo, T., Takeda, Y., Ishino, M., Saotome, K., Kataoka, N.: A mass data update
method in distributed systems. Procedia Computer Science 22, 502{511 (2013)
71. K¨uhn, E., Pohlai, H., Puntigam, F.: Concurrency and backtracking in Vienna
Parallel Logic. Computer Languages 19(3), 185{203 (1993)
72. Lakhal, N.B., Kobayashi, T., Yokota, H.: Ws-sagas: transaction model for reliable
web-services-composition specification and execution (2003)
73. Larrucea, X., Santamaria, I., Software, R.C.P.. . . ., undefined 2018: Microservices.
ieeexplore.ieee.org https://ieeexplore.ieee.org/abstract/document/8354423/
74. Leu, Y., Elmagarmid, A.K., Boudriga, N.: Specification and execution of transactions for advanced database applications. Information Systems 17(2), 171{183
(1992)
75. Lim´on, X., Guerra-Hern´andez, A., S´anchez-Garc´ıa, A.J., Arriaga, J.C.P.: Sagamas: a software framework for distributed transactions in the microservice architecture. In: 2018 6th International Conference in Software Engineering Research
and Innovation (CONISOFT). pp. 50{58. IEEE (2018)
76. Lin, Q., Byun, J.: A compensation cost-aware coordination for distributed long
running transactions. Lecture Notes in Electrical Engineering 280 LNEE, 261{
269 (2014)
77. Liu, A., Liu, H., Li, Q., Huang, L.S., Xiao, M.J.: Constraints-aware scheduling for
transactional services composition. Journal of Computer Science and Technology
24(4), 638{651 (jul 2009)
78. Liu, C., Lin, X., Orlowska, M., Zhou, X.: Confirmation: Increasing resource
availability for transactional workflows. Information Sciences 153(SUPP), 37{53
(2003)
79. Lu, H.: Implementation of an advanced transaction model for an integrated computing environment for building construction (2002)
80. Madria, S.K., Maheshwari, S.N., Chandra, B., Bhargava, B.: Crash recovery in
an open and safe nested transaction model. Tech. rep. (1997)
81. Madria, S.K., Maheshwari, S.N., Chandra, B., Bhargava, B.: Formalization and
proof of correctness of the crash recovery algorithm for an open and safe nested
transaction model. International Journal of Cooperative Information Systems
10(1-2), 1{50 (2001)
82. Mahato, D.P., Singh, R.S.: On maximizing reliability of grid transaction processing system considering balanced task allocation using social spider optimization.
Swarm and Evolutionary Computation 38, 202{217 (2018)
83. Malyuga, K., Perl, O., Slapoguzov, A., Perl, I.: Fault tolerant central saga orchestrator in restful architecture. In: 2020 26th Conference of Open Innovations
Association (FRUCT). pp. 278{283. IEEE (2020)
84. Mamun, Q., Kumar, M.: Commit Protocols in Distributed Database System: A
Comparison Related papers Proxy based T wo Phase Commit wit h Wait for Improved Response T ime and Blocking Probab. . . Commit Protocols in Distributed
Database System: A Comparison. IJIRST-International Journal for Innovative
Research in Science & Technology| 2 (2016)
85. Mancini, L.V., Ray, I., Jajodia, S., Bertino, E.: Flexible transaction dependencies
in database systems. Distributed and Parallel Databases 8(4), 399{446 (2000)
86. Martin, C.P., Ramamritham, K.: Toward Formalizing Recovery of (Advanced)
Transactions. In: Advanced Transaction Models and Architectures, pp. 213{234.
Springer US (1997)
87. Mentis, A., Katsaros, P.: Model checking and code generation for transaction processing software. In: Concurrency Computation Practice and Experience. vol. 24,
pp. 711{722. Wiley Online Library (may 2012)
88. Mihindukulasooriya, N., ESTEBAN-GUTI, M., et al.: A survey of restful transaction models: one model does not fit all. Tech. Rep. 1-2 (2016)
89. Mohan, C., Alonso, G., G¨unth¨or, R., Kamath, M.: Exotica: A Research Perspective on Workflow Management Systems (1995)
90. Mohan, C., Haderle, D., Lindsay, B., Pirahesh, H., Schwarz, P.: ARIES: A Transaction Recovery Method Supporting Fine-Granularity Locking and Partial Rollbacks Using Write-Ahead Logging (1992)
91. Moon, S.J., Lee, S.: A Reliable Nested Transaction Model with Extension of
Real-Time Characteristics. Reliable and Autonomous Computational Science pp.
123{142 (2011)
92. Moravan, M.J., Bobba, J., Moore, K.E., Yen, L., Hill, M.D., Liblit, B., Swift,
M.M., Wood, D.A.: Supporting nested transactional memory in logTM. ACM
SIGARCH Computer Architecture News 34(5), 359{370 (oct 2006)
93. Moss, E.B.: Open Nested Transactions: Semantics and Support (2006)
94. Moss, J.E.B., Hosking, A.L.: Nested transactional memory: Model and architecture sketches. Science of Computer Programming 63(2), 186{201 (2006)
95. Mour~ao, E., Kalinowski, M., Murta, L., Mendes, E., Wohlin, C.: Investigating
the use of a hybrid search strategy for systematic reviews. In: 2017 ACM/IEEE
International Symposium on Empirical Software Engineering and Measurement
(ESEM). pp. 193{198. IEEE (2017)
96. Muth, P., Rakow, T.C., Klas, W., Neuhold, E.J.: A transaction model for an open
publication environment. Tech. Rep. 1 (1991)
97. Newman, S.: Building microservices : designing fine-grained systems. O’Reilly
Media, Inc. (2015)
98. Nguyen, P.H., Kramer, M., Klein, J., Le Traon, Y.: An extensive systematic review
on the Model-Driven Development of secure systems. Information and Software
Technology 68, 62{81 (2015)
99. Norta, A.: Exploring a framework for advanced electronic business transactions
(2008)
100. Pandey, S., Shanker, U.: IDRC: A Distributed Real-Time Commit Protocol. In:
Procedia Computer Science. vol. 125, pp. 290{296. Elsevier B.V. (jan 2018)
101. Pandey, S., Shanker, U.: RAPID: A real time commit protocol. Journal of King
Saud University - Computer and Information Sciences (apr 2020)
102. Papazoglou, M.P.: Web services and business transactions (mar 2003)
103. Park, J., Choi, K.S.: An adaptive coordination framework
for fast atomic multi-business transactions using web services. Decision Support Systems 42(3), 1959{1973 (2006),
http://www.sciencedirect.com/science/article/pii/S016792360600073X
104. Petersen, K., Feldt, R., Mujtaba, S., Mattsson, M.: Systematic mapping studies
in software engineering. In: Ease. vol. 8, pp. 68{77 (2008)
105. Petersen, K., Vakkalanka, S., Kuzniarz, L.: Guidelines for conducting systematic
mapping studies in software engineering: An update. Information and Software
Technology 64, 1{18 (2015)
106. Peterson, C.: Correctness and Progress Verification of Non-Blocking Programs.
Tech. rep. (2004)
107. Peterson, C., Wilson, A., Pirkelbauer, P., Dechev, D.: Optimized transactional
data structure approach to concurrency control for in-memory databases. In: 2020
IEEE 32nd International Symposium on Computer Architecture and High Performance Computing (SBAC-PAD). pp. 107{115. IEEE (2020)
108. Pu, C., Hseush, W., Kaiser, G.E., Wu, K.L., Yu, P.S.: Divergence control for
distributed database systems. Distributed and Parallel Databases 3(1), 85{109
(1995)
109. Pu, C., Leff, A., Chen, S.W.F.: An Evolutionary Path for Transaction Processing
Systems. In: Zelkowitz, M.B.T.A.i.C. (ed.) Advances in Computers, vol. 41, pp.
255{296. Elsevier (1995)
110. Pu, C., Meersman, R., Liu, L.: A transactional activity model for organizing
open-ended cooperative workflow activities (1996)
111. Qu, J., Dong, L., Gui, L., Wang, W., Lan, J.: Research on time performance of
dynamic nested transactions in open reconfigurable network (2013)
112. Rajapakse, J., Orlowska, M.E.: An extended transaction to maintain consistency and recovery in multidatabase systems. Information Sciences 90(1-4), 19{38
(1996)
113. Rajaram, K., Babu, C., Adiththan, A.: Tx-FAITH: A transactional framework
for failure tolerant execution of hierarchical long-running transactions in business
applications (2014)
114. Roller, D., Eck, O., Dalakakis, S.: Integrated version and transaction group model
for shared engineering databases. Data and Knowledge Engineering 42(2), 223{
245 (2002)
115. Rusinkiewicz, M., Sheth, A.: On Transactional Workflows (1993)
116. Rusinkiewicz, M., Sheth, A.: Specification and execution of transactional workflows, Modern Database Systems (1995)
117. Sch¨afer, D.R., Tariq, M.A., Rothermel, K.: Highly availabile process executions.
Institute of Parallel and Distributed Systems, University of Stuttgart, Tech. Rep.
p. 2016 (2016)
118. Sch¨afer, M., Dolog, P., Nejdl, W.: An environment for flexible advanced compensations of Web service transactions (2008)
119. Schewe, K.D., Ripke, T., Drechsler, S.: Hybrid concurrency control and recovery
for multi-level transactions (1999)
120. Schuldt, H., Alonso, G., Beeri, C., Schek, H.J.: Atomicity and Isolation for Transactional Processes (2002)
121. Serrano-Alvarado, P., Roncancio, C., Adiba, M.: A survey of mobile transactions
(sep 2004)
122. Shen, B.J., Chen, C., Ju, D.H.: Criteria-based software process transaction model.
Ruan Jian Xue Bao/Journal of Software 13(1), 24{32 (2002)
123. Stefanko, B.: Use of Transactions within a Reactive Microservices Environment. ˇ
Tech. rep.
124. Stefanko, M., Chaloupka, O., Rossi, B.: The saga pattern in a reactive microservices environment. ICSOFT 2019 - Proceedings of the 14th International Conference on Software Technologies pp. 483{490 (2019)
125. Tang, F., Guo, M.: Grid transaction management and highly reliable grid platform. The Handbook of Research on Scalable Computing Technologies 1, 421{441
(2009)
126. Tang, F., Li, M., Guo, M.: A transactional grid workflow service for ShanghaiGrid
(2007)
127. Tang, F., You, I., Li, L., Wang, C.L., Cheng, Z., Guo, S.: A pipeline-based approach for long transaction processing in web service environments (2011)
128. Torres, J., Ju´arez, E., Dodero, J.M., Aedo, I.: Advanced Transactional Models for
Complex Learning Processes. Recursos Digitales para el Aprendizaje pp. 360{368
(2009)
129. Tran, T., Steffen, M., Truong, H.: Estimating Resource Bounds for Nested Transactions with Join Synchronization. Www-Ps.Informatik.Uni-Kiel.De
130. Turcu, A., Palmieri, R., Ravindran, B.: On Open Nesting in Distributed Transactional Memory (2016)
131. Uyanık, H., Ovatman, T.: Enhancing two phase-commit protocol for replicated
state machines. In: 2020 28th Euromicro International Conference on Parallel,
Distributed and Network-Based Processing (PDP). pp. 118{121. IEEE (2020)
132. Veijalainen, J., Terziyan, V., Tirri, H.: Transaction management for m-commerce
at a mobile terminal. Electronic Commerce Research and Applications 5(3), 229{
245 (2006)
133. Vonk, J., Grefen, P.: Cross-organizational transaction support for e-services in
virtual enterprises. Distributed and Parallel Databases 14(2), 137{172 (sep 2003)
134. Vossen, G.: Transactional workflows. In: International Conference on Deductive
and Object-Oriented Databases. pp. 20{25. Springer (1997)
135. Wang, J., Jin, B., Li, J.: Pessimistic predicate/transform model for long running
business processes. Tsinghua Science and Technology 10(3), 288{297 (2005)
136. Wang, T., Vonk, J., Kratz, B., Grefen, P.: A survey on the history of transaction
management: from flat to grid transactions. Distributed and Parallel Databases
23(3), 235{270 (2008)
137. Wang, X., Dong, L., Gui, L., Wang, W., Lan, J.: Research on the cost of open
nested transaction in the open reconfigurable network (2013)
138. Waseem, M., Liang, P., Shahin, M.: A systematic mapping study on microservices
architecture in devops. Journal of Systems and Software 170, 110798 (2020)
139. Weigand, H., Ngu, A.H.: Flexible specification of interoperable transactions. Data
and Knowledge Engineering 25(3), 327{345 (1998)
140. Weikum, G.: Principles and Realization Strategies of Multilevel Transaction Management (1991)
141. Weikum, G., Hasse, C.: Multi-level transaction management for complex objects:
Implementation, performance, parallelism. The VLDB Journal 2(4), 407{453 (oct
1993)
142. Weikum, G., Schek, H.J.: Concepts and Applications of Multilevel Transactions
and Open Nested Transactions (1992)
143. YAGHMAEI, F.: Content validity and its estimation. Journal of Medical Education Spring 3, 25{27 (2003)
144. Younas, M., Chao, K.M.: A tentative commit protocol for composite web services. Journal of Computer and System Sciences 72(7), 1226{1237 (2006).
https://doi.org/10.1016/j.jcss.2005.12.006
145. Younas, M., Kouadri Most´efaoui, S.: A new model for context-aware transactions
in mobile services. Personal and Ubiquitous Computing 15(8), 821{831 (2011)
146. Yu, G., Guoren, W., Huaiyuan, Z., Taiyong, J., Kaneko, K., Makinouchi, A.: A
non-blocking locking method and performance evaluation on network of workstations. Journal of Computer Science and Technology 16(1), 25{38 (2001)
147. Zhang, H., Zhao, W.: Web Services Coordination for Business Transactions. Encyclopedia of Information Science and Technology, Second Edition pp. 4070{4076
(2011)
148. Zhao, W., Kart, F., Moser, L.E., Melliar-Smith, P.M.: A reservation-based extended transaction protocol for coordination of web services. International Journal of Web Services Research 5(3), 64{95 (2008)
149. Zhao, W., Moser, L.E., Melliar-Smith, P.M.: A reservation-based extended transaction protocol. IEEE Transactions on Parallel and Distributed Systems 19(2),
188{203 (feb 2008)
